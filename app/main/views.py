import json
from datetime import datetime,time
from flask import Blueprint, abort
from flask_restful import Resource
from flask_restful import reqparse
from app.main.models import Usuario,Transferencia
from app import api, db



catalog = Blueprint('catalog', __name__)

#Parse Usuario
usr = reqparse.RequestParser()
usr.add_argument('nome', type=str)
usr.add_argument('cnpj', type=str)


#Parse Transferencia
transf = reqparse.RequestParser()
transf.add_argument('usuario', type=int)
transf.add_argument('pagador_nome', type=str)
transf.add_argument('pagador_banco',type=str)
transf.add_argument('pagador_agencia', type=str)
transf.add_argument('pagador_conta',type=str)
transf.add_argument('beneficiario_nome', type=str)
transf.add_argument('beneficiario_banco',type=str)
transf.add_argument('beneficiario_agencia', type=str)
transf.add_argument('beneficiario_conta',type=str)
transf.add_argument('valor',type=float)

get_transferencia = reqparse.RequestParser()
get_transferencia.add_argument('data_transferencia',type=str)
get_transferencia.add_argument('pagador_nome',type=str)
get_transferencia.add_argument('beneficiario_nome',type=str)


@catalog.route('/')
@catalog.route('/home')
def home():
    return "Welcome to the NIX API."


class TransferenciaAPI(Resource):

    def get(self, page=1):

        total_transacoes = 0.
        data_transferencia = None
        pagador_nome =  None
        beneficiario_nome = None

        args = get_transferencia.parse_args()
        data_transferencia = args['data_transferencia']
        pagador_nome = args['pagador_nome']
        beneficiario_nome = args['beneficiario_nome']

        if not data_transferencia or not pagador_nome or not beneficiario_nome:
            transferencias = Transferencia.query.filter_by(ativo=True).paginate(page, 10).items
        else:
            transferencias = Transferencia.query.filter_by(data_transferencia=data_transferencia,
                                                           pagador_nome=pagador_nome,
                                                           beneficiario_nome=beneficiario_nome,
                                                           ativo=True).all()
        if not transferencias:
            abort(404)

        res = {}
        for transferencia in transferencias:
            res[transferencia.id] = {
                'usuario': transferencia.usuario.id,
                'pagador_nome': transferencia.pagador_nome,
                'pagador_banco': transferencia.pagador_banco,
                'pagador_agencia': transferencia.pagador_agencia,
                'pagador_conta': transferencia.pagador_conta,
                'beneficiario_nome': transferencia.beneficiario_nome,
                'beneficiario_banco': transferencia.beneficiario_banco,
                'beneficiario_agencia': transferencia.beneficiario_agencia,
                'beneficiario_conta': transferencia.beneficiario_conta,
                'valor': str(transferencia.valor),
                'tipo': transferencia.tipo,
                'status': transferencia.status,
                'ativo': transferencia.ativo,
                'data_transferencia': str(transferencia.data_transferencia)
            }
            total_transacoes = total_transacoes + float(transferencia.valor)

        res['total'] = {
            'numero_transacoes':str(len(transferencias)),
            'total_transferencias': str(total_transacoes)

        }
        return json.dumps(res)

    def post(self):

        now = datetime.now()

        status = None
        tipo = None
        args = transf.parse_args()
        usuario = args["usuario"]
        pagador_nome = args['pagador_nome']
        pagador_banco = args['pagador_banco']
        pagador_agencia = args['pagador_agencia']
        pagador_conta = args['pagador_conta']
        beneficiario_nome = args['beneficiario_nome']
        beneficiario_banco = args['beneficiario_banco']
        beneficiario_agencia = args['beneficiario_agencia']
        beneficiario_conta = args['beneficiario_conta']
        valor = args['valor']
        ativo = True

        data = datetime.now()
        data = data.strftime("%d/%m/%Y")
        data_transferencia = data


        if str(pagador_banco) == str(beneficiario_banco):
            tipo = 'CC'
        elif (time(16,00) <= now.time() <= time(10,00)) and (valor < 5000.):
            tipo = 'TED'
        else:
            tipo = 'DOC'


        if valor > 100000.:
            status = 'ERRO'
        else:
            status = 'OK'

        transferencia = Transferencia(usuario,
                                     pagador_nome,
                                     pagador_banco,
                                     pagador_agencia,
                                     pagador_conta,
                                     beneficiario_nome,
                                     beneficiario_banco,
                                     beneficiario_agencia,
                                     beneficiario_conta,
                                     valor,
                                     tipo,
                                     status,
                                     data_transferencia,
                                     ativo
                                      )

        db.session.add(transferencia)
        db.session.commit()

        res = {}
        res[transferencia.id] = {
            'usuario': transferencia.usuario.id,
            'pagador_nome': transferencia.pagador_nome,
            'pagador_banco': transferencia.pagador_banco,
            'pagador_agencia': transferencia.pagador_agencia,
            'pagador_conta': transferencia.pagador_conta,
            'beneficiario_nome': transferencia.beneficiario_nome,
            'beneficiario_banco': transferencia.beneficiario_banco,
            'beneficiario_agencia': transferencia.beneficiario_agencia,
            'beneficiario_conta': transferencia.beneficiario_conta,
            'valor': str(transferencia.valor),
            'tipo': transferencia.tipo,
            'status': transferencia.status,
            'ativo': transferencia.ativo
        }
        return json.dumps(res)

    def delete(self,id=None):

        if id is not None:
            transferencia = Transferencia.query.get_or_404(id)
            transferencia.ativo = False

            db.session.add(transferencia)
            db.session.commit()
        else:
            abort(404)

        return {}, 200

api.add_resource(
    TransferenciaAPI,
    '/api/transferencia',
    '/api/transferencia/<int:id>'
)


class UsuarioAPI(Resource):

    def get(self, id=None, page=1):
        if not id:
            usuarios = Usuario.query.paginate(page, 10).items
        else:
            usuarios = [Usuario.query.get(id)]
        if not usuarios:
            abort(404)
        res = {}
        for usuario in usuarios:
            res[usuario.id] = {
                'nome': usuario.nome,
                'cnpj': usuario.cnpj,
            }
        return json.dumps(res)


    def post(self):
        args = usr.parse_args()
        nome = str(args["nome"])
        cnpj = str(args['cnpj'])
        usuario = Usuario(nome,cnpj)
        db.session.add(usuario)
        db.session.commit()

        res={}
        res[usuario.id] = {
            'nome': usuario.nome,
            'cnpj': usuario.cnpj,
        }
        return json.dumps(res)

api.add_resource(
    UsuarioAPI,
    '/api/usuario'
)