from app import db


class Usuario(db.Model):
    __tablename__ = "usuario"
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(128))
    cnpj = db.Column(db.String(14))

    def __init__(self, nome, cnpj):
        self.nome = nome
        self.cnpj = cnpj

    def __repr__(self):
        return '<Usuario %d>' % self.id


class Transferencia(db.Model):
    __tablename__  = "transferencia"

    id = db.Column(db.Integer, primary_key=True)

    # Relacionamento
    usuario_id = db.Column(db.Integer, db.ForeignKey('usuario.id'))
    usuario = db.relationship("Usuario")

    pagador_nome = db.Column(db.String(128))
    pagador_banco = db.Column(db.String(3))
    pagador_agencia = db.Column(db.String(4))
    pagador_conta = db.Column(db.String(6))
    beneficiario_nome = db.Column(db.String(128))
    beneficiario_banco = db.Column(db.String(3))
    beneficiario_agencia = db.Column(db.String(4))
    beneficiario_conta = db.Column(db.String(6))
    valor = db.Column(db.Numeric(precision=15, scale=2))
    tipo = db.Column(db.String(4))
    status = db.Column(db.String(12))
    data_transferencia = db.Column(db.String(10))
    ativo = db.Column(db.Boolean, default=True)


    def __init__(self, usuario,
                 pagador_nome,
                 pagador_banco,
                 pagador_agencia,
                 pagador_conta,
                 beneficiario_nome,
                 beneficiario_banco,
                 beneficiario_agencia,
                 beneficiario_conta,
                 valor,
                 tipo,
                 status,
                 data_transferencia,
                 ativo):

        self.usuario_id = usuario
        self.pagador_nome = pagador_nome
        self.pagador_banco = pagador_banco
        self.pagador_agencia = pagador_agencia
        self.pagador_conta = pagador_conta
        self.beneficiario_nome = beneficiario_nome
        self.beneficiario_banco = beneficiario_banco
        self.beneficiario_agencia = beneficiario_agencia
        self.beneficiario_conta = beneficiario_conta
        self.valor = valor
        self.tipo = tipo
        self.status = status
        self.data_transferencia = data_transferencia
        self.ativo = ativo

    def __repr__(self):
        return '<Transferencia %d>' % self.id


